# Synthèse codes utile en perl

Synthèse des codes utiles basiques et bonnes pratiques en perl pour un cours d'introduction à la programmation et au langage perl à l'UCLouvain (LFIAL2630 - Méthodologie du traitement informatique des données textuelles, 2019)

## Au début du code

### shebang

``#! /usr/bin/env perl``

### Commentaire descriptif

Exemple :

```perl
#============================================================
# Script : multiplication.pl
# Auteur : Name Surname <user@mail.com>
# Date : 21/02/2018
#============================================================
# Ce script prend en arguments deux nombres et renvoie
# le resultat de la multiplication de l'un par l'autre.
#============================================================
# Usage :
# perl multiplication.pl <nombre> <nombre>
#============================================================
```


## type de variables

1. **scalaires** (str, int, float) ``$variable``
2. **tableau** ``my @tableau = ( 1 .. 10 );`` et pour accéder à elem : ``$tableau[2] == 3``
	1. tri : (ordre alphanumérique)
		1. ``reverse( @tab )`` inverse les éléments d'une liste
		2. ``sort( @tab )`` trie la liste en ordre croissant
		3. ``revers( sort( @tab ) )`` trie en ordre décroissant
	2. manipulation :
		1. ``shift( @tab )`` : supprimer le 1e elem du tableau et le retourner
		2. ``unshift( @tab, $premelem );`` : mettre un elem en début de tableau
		3. ``pop( @tab )`` : supprimer le dernier elem et le retourner
		4. ``push( @tab, $dernierelem )`` : ajouter un elem à la fin
	3. split et join :
		1. ``my @tab = split( / / , "Kevin Georges citron" );`` crée un tableau à partir d'un caractère de séparation (ici espace) d'une chaîne (+ d'info avec les expression régulières)
		2. ``my $chaine = join( " " ,@tab );`` join les élément d'un tableau en un str, avec la caractère choisi comme séparateur
3. **table de hachage** :
	1. ``my %hash = ( "pommes" => 3);`` ou
	2. ``my %hash = ( "pommes",3);`` ou
	3.

	```perl
	my %hash = ();
	$hash{"pommes"} = 3;
	```

	4. manipulations :
		1. ``delete( $panier{"poires"} );`` supprimer toutes les références
		2. ``$panier{"mandarines"} = undef;`` ajouter une catégorie vide
		3. ``keys( %hash );`` revoit toutes les clés du hash
		4. ``values(%hash)`` : renvoit les valeurs
		5. ``my ( $cle, $valeur ) = each( %panier )``

			exemple :
```perl
my %panier = ( "pommes" => 3, "bananes" => 4, "poires" => 4 );
while ( my ( $cle, $valeur ) = each( %panier ) ) {
	print $cle . "\t" . $valeur . "\n";
}
```

		6. ``exists($hash{$key})`` indique s’il existe une clé $key dans une table de hachage $hash
4. Les **références** : ``my $ref1 = \$scalaire;``, ``my $ref2 = \@liste;``, ``my $ref3 = \%hachage;`` pour passer un tableau ou un hache à une fonction ou dans un tableau ou un autre hache
	1. On utilise le caractère « \ » pour indiquer que l’on veut utiliser une référence à la variable plutôt que la variable elle-même.
	2. Accéder au contenu de la reférence, *déréférencer* :
		1. ``my $scalaire_d = ${$ref1};`` pour déréférencer un scalaire
		3. ``my @liste_d = @{$ref2};`` pour déréférencer une liste
		4. ``my %hachage_d = %{$ref3};`` pour déréférencer un hachage
	5. ``ref($ref)`` renvoie le type de la variable à laquelle cette référence réfère
	6. contruction :
		1. direct :
			1. tableau : ``my $lettres = [ 'a', 'b', 'c' ];``
			2. hash : ``my $fruits = { 'pommes' => 3, 'poires' => 4 };``
		2. indirecte : `` my @lettres = ( 'a', 'b', 'c' );`` puis ``my $lettres = \@lettres;``
	7. accès à élém par référence pour ``my $lettres = [ 'a', 'b', 'c' ];``. On peut modifier les éléments par référence
		1. difficile : ``${ $lettres }[0];``
		2. facile à lire : ``$lettres->[0];`` ou ``$fruits->{'pommes'};`` pour un hash

Booléens ? pas exactement mais :
- 0 , ``undef`` et "" (rien) représentent une valeur fausse
- n’importe quelle autre valeur ( 17 ou "pommede terre" ) est vraie

### Enregistrer une structure de données dans un fichier et réutiliser la structure

#### use Storable

**Storable
a deux gros défauts :
• Le fichier qu’il produit n’est pas lisible par un humain
• Ce fichier ne peut être rouvert que sur une même architecture
matérielle**

On préférera les alternatives suivantes si on cherche plus de flexibilité :

- **JSON::MaybeXS** pour la lisibilité et la portabilité
- **YAML** pour la lisibilité et la portabilité
- **Sereal** pour la portabilité, la compression et la vitesse

(à installer, contrairement à Storable)

Enregistrer

```perl
use strict;
use Storable;

my %fruits = ('pommes'=> 12 , 'poires'=>17 , 'abricots' => 4 , 'raisins' => 5);

store(\%fruits,'mon_fichier.db'); # enregistrer
```

Récupérer

```perl
my $fruits_ref = retrieve('mon_fichier.db'); # lire et récupérer les données sous forme de référence

while ( my ( $cle, $valeur ) = each( %{$fruits} ) ) {
	print $cle . "\t" .$valeur . "\n";
}
```

### Autovivification

générer automatiquement toutes les structures de données simples (tableaux, hachages) nécessaires à la construction des données complexes
exemple :

```perl
my $caddie = {}; # le cadie est vide

$caddie->{'food'}->{'legumes'}->{'carottes'} = 3;

$caddie->{'food'}->{'fruits'}->{'pommes'} = 12;


print $caddie->{'food'}->{'fruits'}->{'pommes'};
```

## récupérer arguments de commande

Les arguments sont de scalaires stockés dans un tableau

```perl
$valeur1 = $ARGV[0];

# ou bien :

$valeur1 = shift();
```

## If - else - else if

```perl
if ( ... ){
	...
} elsif ( ... ){
	...
} else {
	...
}

```

### Comparaisons

1) valeurs numériques:
``< `` ``>`` ``==`` ``<=`` ``>=`` ``!=``

2) chaînes

```perl
lt 	(lesser than)
gt	(greater than)
eq	(equal)
le	(lesser or equal to)
ge	(greater or equel)
ne	(not equel)
```

### opérateurs logique ("and" "or" "not")

- ``and`` ou ``&&``
- ``or``  ou ``||``
- ``ǹot`` ou ``!``

→ utiliser forme lgg nat (and or not) plutôt que les symboles moins lisibles (légère différence au niveau de la précédence, voir slides)

```perl
if ((...) and (...)){}
```

## la boucle while

```perl
while ( ... ){ ... }
```

## boucle for

- utile qd on connait le nombre de données à parcourir (parcorir un tableau)
- !! pas adaptée pour la lecture des lignes d’un fichier

```perl
for ( my $count = 1 ; $count <= 5 ; $count++ ) {
}
```

Note : pour parcourir un tableau avec la boucle for, c'est bien simple :

```perl
for ( my $i = 0 ; $i < @tab ; $i++ ) { ... } #perl comprends dan le contexte qu'on demande la taille du tableau
```

ou bien

```perl
for my $elem ( @tab ) { } #tendance perl 6, ressemblance à boucel foreach
```

## boucle foreach

```perl
foreach my $elem ( @tab ) { }
```

## incrémenter

``$compteur+= 1;`` ou ``$compteur++;`` ou ``++$compteur;``
( différent de $compteur++; dans certains contextes)

## Gestion de fichier
(il existe une version à 2 arguments mais moins sécurisée)

Possibiliter d'utiliser:

- un *filehandle* : ``FICHIER`` -> mais problème si on ouvre un autre fichier avec même nom ! (Pb de sécurité aussi)
- une variable scalaire : ``$fichier`` (mieux !)

### lire (ouvrir)

```perl
open( my $fichier, '<', 'monfichier.txt' )
	or die( "Impossible d'ouvrir mon_fichier.txt" );

while ( my $ligne = <$fichier> ) {
	print $ligne;
}
close $fichier;

```

#### manipulation :

##### mettre chaque ligne d'un fichier dans un tableau

``my @text = <$fichier>;``(plus facile, plus court, mais tout le fichier est chargé en mémoire)

ou alors avec une boucle while (plus long)

```perl
my @text = ();
while ( my $ligne = <$fichier> ) {
	push( @text, $ligne );
}
```

### écrire

```perl
open( FICHIER, ">", "mon_fichier.txt")
	or die( "Impossible d'ouvrir mon_fichier.txt" );
```

### réécrire

```perl
open( FICHIER, ">>", "mon_fichier.txt")
	or die( "Impossible d'ouvrir mon_fichier.txt" );
```


## Ouvrir un répertoire et accéder à son contenu

```perl
opendir(my $dir,"mon_repertoire") or die("...");

my $file = readdir($dir);
while ($file = readdir($dir)) {
	if ( ( $fichier ne '.' ) && ( $fichier ne '..' ) ) {
	# pour ne pas prendre en compte les 2 fichiers spéciaux . et .., qui sont à éviter dans la plupart des cas.

	# mes_instructions ;
	}
}
closedir($dir);
```

## des séquence d'échapement pour les str

- ``\n`` Nouvelle ligne
- ``\r`` Retour au début de la ligne courante
- ``\t`` Tabulation horizontale
- ``\f`` Tabulation verticale

## Les sorties et entrées en perl

### Sortie standard, sortie d'erreur

s'utilisent comme des filehandler

- ``STDOUT`` : sortie standard (dépfault de print())
- ``STDERR`` : la sortie d'erreur

utilisation :

- ``print STDERR ($texte);`` imprime à l'écran un msg d'erreur
- ``perl monprog.pl >/dev/null`` cache la sortie standard
- ``perl prog.pl 2>errors.txt`` imprime la sortie d'erreur dans un fichier txt

### entrée standard

``my $data = <STDIN>;``

#### opérateur diamant <> et <<>>

``<<>>`` est une version plus récente et plus sécurisée que ``<>``

- si pas d'arguments donné : fonctionne exactement comme ``<STDIN>``
- sinon : considère que chaque nom passé en argument est un fichier, ouvre les fichiers un par un et accède à leur contenu. La variable ``$ARGV`` contient le nom du fichier en cours de lecture.

## Les expressions régulières

voir momento

## quelques fonctions de base :

- ``rand(X)`` : renvoie un nombre aléatoire réel compris entre 0 et X non inclus
- ``int(X)``	  : renvoie la partie entière d’un nombre réel
- ``sleep(secondes)``
- ``chomp($chaine);`` : enlève le(s) caractère(s) de retour à la ligne à la fin d’une chaîne de caractères
- ``qw(elem1 elem lmk) #=> ("elem1", "elem", "lmk")`` : permet de faire un tableau sans taper les guillemets et les virgules (séparateur = ' ')
- ``select()`` : changer la sortie de la fonction ``print()``
- ``ref($ref)`` renvoie le type de la variable à laquelle cette référence réfère

### créer fonction (= subroutines)

La fonction peut être à la fin du code et appelée au début, ça n'a pas d'importance.

Un fonction reçoit le tableau ``@_`` comme argument. Donc pas moyen d'envoyer directement un tableau ou un hash en argument (utiliser les références !). Mais possible d'en retourner avec ``return``

exemple :

```perl
sub printn {
	# my $texte = $_[0]; #récupérer un argument
	# ou avec shift:
	my $texte = shift;
	print $texte . "\n";
	return $texte;
}
```

#### passer autre chose que des scalaires en argument de fonction :

## quelques pragma (modules) utiles :

- ``use strict;`` : oblige le programmeur à
	+ déclarer toutes ses variables à l’aide d’un ``my``
	+ deux autres de ses effets concernent les **références** et **routines**
- ``use warnings;``
- ``use diagnostics;`` : explicite les messages d’erreur affichés par l’interpréteur perl
- ``use FindBin;`` fournit une variable scalaire, $FindBin::Bin dans laquelle se trouve le chemin qui mène au script.
- ``use lib '.'`` indiquer à Perl que le répertoire courant doit être considéré comme un répertoire qui contient des modules utilisables.

On peut trouver des tas de modules sur le site du CPAN http://www.cpan.org

Instruction d'install sur slides

### Créer son module perl

extension de fichier **.pm**

structure du fichier :

```perl
package mon_module; # = nom du fichier, sans .pm
use strict;

# variables;
# eventuellement : instructions d'initialisation;

sub ma_fonction_1 { instructions }
sub ma_fonction_2 { instructions }
1; # un module se termine toujours par 1 !!!! permet d'indiquer au programme appelant que le module est fini et lui laisser reprendre la main
```

### utiliser son propre module :

1. ``use FindBin;``
2. ``use lib 'FindBin::Bin';`` pour indiquer que le module se trouve dans le même dossier que le script
2. Déclarer que l’on utilise tel module : ``use mon_module;``
2. Nommer le module quand on utilise une de ses fonctions : ``my $line = TextualData::cleanline($line)``
